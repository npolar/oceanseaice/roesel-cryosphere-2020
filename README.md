## Implications of surface flooding on airborne thickness measurements of snow on sea ice

A repository for code supporting the publication 'Implications of surface flooding on airborne thickness measurements of snow on sea ice' (https://doi.org/10.5194/tc-2020-168).

Please contact Anja Roesel for data. The notebook(s) here are adapted by Adam Steer, from code supplied by multiple authors.

- `roesel-2020-imports` contains code for lat/lon to polar projections. It is provided here for historical reference. In the notebooks supplied, coordinate transformations are done with `pyproj`
- `roesel-etal-workingcode.ipynb` is a jupyter notebook showing how figures, plots and statistics are generated.

Please log issues in this repository if you find mistakes or have questions - given the nature of research work, authors may or may not be contactable at the institutions listed, and may or may not have time to continue this work. This represents a minimum viable working state - just enough work to get figures and analysis done, there was no time to make everything clean and tidy.
