#module general/polar_projection.py
# LK 14/08/2008,4/2/2009

#from numpy import *
import numpy as np



# Polar stereographic (NSDIDC grid)
def mapxy(x, y, sgn):
    cdr  = 57.29577951
    slat = 70.0
    re   = 6378.273
    ec2   = .006693883
    ec    =  np.sqrt(ec2)
    if sgn == 1:
        delta = 45.0
    else:
        delta = 0.0
    sl = slat * np.pi/180.0
    rho = np.sqrt(x**2 + y**2)
    cm = np.cos(sl) / np.sqrt(1.0 - ec2 * (np.sin(sl)**2))
    t = np.tan((np.pi / 4.0) - (sl / 2.0)) / ((1.0 - ec * np.sin(sl)) / (1.0 + ec * np.sin(sl)))**(ec / 2.0)
    if  (np.absolute(slat-90.0) < 1.e-5):
        t = rho * np.sqrt((1. + ec)**(1. + ec) * (1. - ec)**(1. - ec)) / 2. / re
    else:
        t = rho * t / (re * cm)
    chi = (np.pi / 2.0) - 2.0 * np.arctan(t)
    alat = chi + ((ec2 / 2.0) + (5.0 * ec2**2.0 / 24.0) + (ec2**3.0 / 12.0)) * np.sin(2 * chi) + ((7.0 * ec2**2.0 / 48.0) + (29.0 * ec2**3 / 240.0)) *np.sin(4.0 * chi)+ (7.0 * ec2**3.0 / 120.0) * np.sin(6.0 * chi)
    alat = sgn * alat
    along = np.arctan2(sgn * x, -sgn * y)
    along = sgn * along

    along = along * 180. /np.pi
    alat  = alat * 180. / np.pi
    along = along - delta
    return [alat,along]

def mapll(lat,lon,sgn):
    cdr  = 57.29577951
    slat = 70.
    re   = 6378.273
    ec2   = .006693883
    ec    =  np.sqrt(ec2)
    if sgn == 1:
        delta = 45.0
    else:
        delta = 0.0
    latitude  = np.absolute(lat) * np.pi/180.
    longitude = (lon + delta) * np.pi/180.
    t =  np.tan(np.pi/4-latitude/2)/((1-ec*np.sin(latitude))/(1+ec*np.sin(latitude)))**(ec/2)
    if ((90-slat) < 1.e-5):
        rho = 2.*re*t/np.sqrt((1.+ec)**(1.+ec)*(1.-ec)**(1.-ec))
    else:
        sl  = slat*np.pi/180.
        tc  = np.tan(np.pi/4.-sl/2.)/((1.-ec*np.sin(sl))/(1.+ec*np.sin(sl)))**(ec/2.)
        mc  = np.cos(sl)/np.sqrt(1.0-ec2*(np.sin(sl)**2))
        rho = re*mc*t/tc

    y=-rho*sgn*np.cos(sgn*longitude)
    x= rho*sgn*np.sin(sgn*longitude)
    return [x,y]

def  mapll2(lat, lon):
    if lat>0:
        sgn=1
    else:
        sgn=-1
    return mapll(lat,lon,sgn)

def dist(lat1,lon1,lat2,lon2):
    x1,y1=mapll2(lat1,lon1)
    x2,y2=mapll2(lat2,lon2)
    #print x1,y1,x2,y2
    d=np.sqrt((x2-x1)**2+(y2-y1)**2)
    if sign(lat1)!=sign(lat2):
        d=10000.0
    return d
